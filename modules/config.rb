module Backube
  class Config < SubThor
    include Backube::SharedMethods

    SUBCOMMAND = "config"
    DESCRIPTION = "has some config methods"

    desc "service", "creates service conf for kubernetes."
    method_option :o, :desc => 'Output directory.'

    def service
      file = 'templates/service.yml.erb'
      ERB.new(File.read(file)).result(binding)
    end


    desc 'deploy_to_localhost [FILE]',
         'Transform and deploy server configuration files to the local file system based on ERB templates in config/server'
    method_option :force, :default => false, :desc => 'Overwrite and execute @post commands even if files would not change'

    def deploy_to_localhost(file_pattern='config/breeze/configs/**/*')
      Dir[file_pattern].sort.each do |path|
        transform_and_deploy(path, options[:force]) unless File.directory?(path)
      end
    end


    no_commands do
      def test
        puts '2'
      end
    end
  end
end
