# -*- encoding: utf-8 -*-
$:.push File.expand_path("../backube")
require "backube"

Gem::Specification.new do |s|
  s.name        = "backube"
  s.version     = Backube::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["Daniel Schlegel"]
  s.email       = ["daniel.schlegel@gmx.net"]
  s.homepage    = "https://github.com/firmasaga/backube"
  s.summary     = %q{Thor tasks to manage kubernetes configs and deployments}
  s.description = <<-END_DESCRIPTION
Backube makes it easy to automate kubernetes configuration.
END_DESCRIPTION

  s.rubyforge_project = "backube"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  s.add_dependency('thor')
  s.add_dependency('fog')
  s.add_development_dependency "cucumber"
  s.add_development_dependency "aruba"
  s.add_development_dependency "rspec"
  s.add_development_dependency "factory_girl"
  s.add_development_dependency "factories"
  s.add_development_dependency "simplecov"
  s.add_development_dependency "coveralls"
  s.add_development_dependency "webmock"
end


# group :development do
#   gem 'guard-rspec'
#   gem 'pry'
#   platforms :ruby_21 do
#     gem 'pry-byebug'
#   end
#   platforms :ruby_19, :ruby_20 do
#     gem 'pry-debugger'
#     gem 'pry-stack_explorer'
#   end
# end
#
# group :test do
#   gem 'childlabor'
#   gem 'coveralls', '>= 0.5.7'
#   gem 'json', '< 2' # This is to support Ruby 1.8 and 1.9
#   gem 'tins', '< 1.7' # This is to support Ruby 1.8 and 1.9
#   gem 'addressable', '~> 2.3.6', :platforms => [:ruby_18]
#   gem 'webmock', '>= 1.20', '< 2' # This is to support Ruby 1.8 and 1.9.2
#   gem 'mime-types', '~> 1.25', :platforms => [:jruby, :ruby_18]
#   gem 'rspec', '>= 3'
#   gem 'rspec-mocks', '>= 3'
#   gem 'rubocop', '>= 0.19', :platforms => [:ruby_20, :ruby_21]
#   gem 'simplecov', '>= 0.9'
#   gem 'rest-client', '~> 1.6.0', :platforms => [:jruby, :ruby_18]
# end