require 'helper'
require 'fileutils'

describe Backube do
  before do
    @backube = Backube::Base.new
    FileUtils.mkpath('tmp/config')
    FileUtils.mkpath('tmp/generated')
  end

#   describe "services" do
#     before do
#       @config = <<-SERVICE
# apiVersion: v1
# kind: Service
# metadata:
#   name: frontend
#   labels:
#     name: frontend
#   annotations:
#     DOMAINS: domain.com
# spec:
#   ports:
#     - name: frontend
#       port: 80
#       targetPort: frontend
#   selector:
#     name: frontend
#       SERVICE
#     end
#
#     it "should generate kubernetes config for service" do
#       config = @backube.invoke(:config, [:service], n: 'frontend', p: 80, d: 'domain.com')
#       expect(config).to eq(@config)
#     end
#   end

  describe "replication controller" do
    before do
      @replication_controller = File.read('spec/expected/test.rc.yml')
      @first_service = File.read('spec/expected/first.svc.yml')
      @second_service = File.read('spec/expected/second.svc.yml')
      @backube.invoke(:config, f: 'spec/given/test.yml', o: 'tmp/generated')
    end

    it "should generate kubernetes config for replication controller" do
      expect(File.read(out_file)).to eq(@replication_controller)
    end
    
    it "should generate kubernetes config for first service" do
      expect(File.read(out_file)).to eq(@first_service)
    end
    
    it "should generate kubernetes config for second service" do
      expect(File.read(out_file)).to eq(@second_service)
    end
  end

  after do
    FileUtils.rmtree("tmp/*")
  end

end
 