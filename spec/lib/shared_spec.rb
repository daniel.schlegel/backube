require 'helper'

describe Backube do
  before do
    @rc = Backube::RC.new(File.read('spec/given/test.yml'))
  end
 
  describe "replication controller" do
    before do
      @replication_controller = File.read('spec/expected/test.rc.yml')
      @first_service = File.read('spec/expected/first.svc.yml')
      @second_service = File.read('spec/expected/second.svc.yml')
    end

    it "should generate kubernetes config for replication controller" do
      expect(@rc.rc).to eq(@replication_controller)
    end

    it "should generate kubernetes config for first service" do
      expect(@rc.svcs.first).to eq(@first_service)
    end

    it "should generate kubernetes config for second service" do
      expect(@rc.svcs.last).to eq(@second_service)
    end
  end
end
 