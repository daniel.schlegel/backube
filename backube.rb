#!/usr/bin/env ruby
require "thor"
require 'active_support/concern'

module Backube
  VERSION = 0.1
  class SubThor < Thor
    def self.banner(command, namespace = nil, subcommand = false)
      sans_help = ARGV[0] == "help" ? ARGV[1] : ARGV[0]
      "#{basename} #{sans_help} #{command.usage}"
    end
  end

  $modules = []
  CLASS_REGEX = /class\s(\w+)\s+\<\s+\SubThor/
  begin
    # load shared libraries
    Dir.glob('./lib/*.rb').each do |file|
      require file
    end

    # load module code
    Dir.glob('./concerns/*.rb').each do |file|
      require file
    end

    # load modules
    Dir.glob('./modules/*.rb').each do |mod_file|
      require mod_file
      $modules << open(mod_file) do |file|
        file.grep(CLASS_REGEX) do |line|
          class_name = line.match(CLASS_REGEX)[1]
          self.const_get(class_name)
        end
      end
    end
  rescue StandardError => e
    puts "Unable to load module: #{e.message}"
  end

  class Base < Thor
    desc "config", "creates service and rc conf for kubernetes from inputfile."
    method_option :f, :desc => 'Input file.'
    method_option :o, :desc => 'Output directory.'

    def config

      file = 'templates/service.yml.erb'
      ERB.new(File.read(file)).result(binding)
    end

    no_commands do
      def test
        puts '1'
      end
    end

    # load in the subcommands
    $modules.each do |mod_array|
      mod_array.each do |klass|
        begin
          desc "#{klass::SUBCOMMAND} SUBCOMMAND", klass::DESCRIPTION
          subcommand klass::SUBCOMMAND, klass
        rescue StandardError => e
          puts "Unable to load subcommand: #{e.message}"
        end
      end
    end
  end
end

Backube::Base.start(ARGV)
